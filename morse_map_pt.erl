% Copyright (C) 2019, Florent Gallaire <f@gallai.re>
% MIT license

-module(morse_map_pt).
-export([parse_transform/2]).

parse_transform(Ast, _Opt) -> Morse = morse(lists:nth(4, Ast)),
	                      [parse(Tuple, Morse) || Tuple <- Ast].

morse({function, _, morse, 1, [{_, _, _, _, [{_, _, _, Map}, _]}]}) ->
        element(2, erl_eval:expr(Map, erl_eval:new_bindings())).

parse({function, Line, decode, 2, Clauses}, Morse) ->
	{function, Line, decode, 2, [parse(Clause, Morse) || Clause <- Clauses]};

parse({clause, L, [{_, _, [{_, _, {_, _, {_, _, morse}, [{_, _, Val}]}, _, D}|T1]}|T2], Guard, Expr}, Morse) ->
        Bitstring = maps:get(Val, Morse),
        Size = bit_size(Bitstring),
        <<Int:Size>> = Bitstring,
        {clause, L, [{bin, L,[{bin_element, L, {integer, L, Int}, {integer, L, Size}, D}|T1]}|T2], Guard, Expr};

parse(Tuple, _) -> Tuple.
