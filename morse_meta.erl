% Copyright (C) 2019, Florent Gallaire <f@gallai.re>
% MIT license

-module(morse_meta).
-export([main/1, morse/1, code/1, encode/1, decode/1]).
-compile({parse_transform, morse_meta_pt}).

morse(Char) -> Morse = #{ $0 => <<2#1110111011101110111:19>>,
            $1 => <<2#10111011101110111:17>>,
            $2 => <<2#101011101110111:15>>, 
            $3 => <<2#1010101110111:13>>,
            $4 => <<2#10101010111:11>>,
            $5 => <<2#101010101:9>>,
            $6 => <<2#11101010101:11>>,
            $7 => <<2#1110111010101:13>>,
            $8 => <<2#111011101110101:15>>,
            $9 => <<2#11101110111011101:17>>,

            $A => <<2#10111:5>>,
            $À => <<2#101110111010111:15>>,
            $B => <<2#111010101:9>>,
            $C => <<2#11101011101:11>>,
            $Ç => <<2#1110101110101:13>>,
            $D => <<2#1110101:7>>,
            $E => <<2#1:1>>,
            $É => <<2#10101110101:11>>,
            $È => <<2#1011101010111:13>>,
            $F => <<2#101011101:9>>,
            $G => <<2#111011101:9>>,
            $H => <<2#1010101:7>>,
            $I => <<2#101:3>>,
            $J => <<2#1011101110111:13>>,
            $K => <<2#111010111:9>>,
            $L => <<2#101110101:9>>,
            $M => <<2#1110111:7>>,
            $N => <<2#11101:5>>,
            $O => <<2#11101110111:11>>,
            $P => <<2#10111011101:11>>,
            $Q => <<2#1110111010111:13>>,
            $R => <<2#1011101:7>>,
            $S => <<2#10101:5>>,
            $T => <<2#111:3>>,
            $U => <<2#1010111:7>>,
            $V => <<2#101010111:9>>,
            $W => <<2#101110111:9>>,
            $X => <<2#11101010111:11>>,
            $Y => <<2#1110101110111:13>>,
            $Z => <<2#11101110101:11>>,

            $.  => <<2#10111010111010111:17>>,
            $,  => <<2#1110111010101110111:19>>,
            $:  => <<2#11101110111010101:17>>,
            $?  => <<2#101011101110101:15>>,
            $'  => <<2#1011101110111011101:19>>,
            $-  => <<2#111010101010111:15>>,
            $"  => <<2#101110101011101:15>>,
            $@  => <<2#10111011101011101:17>>,
            $!  => <<2#1110101110101110111:19>>,
            $;  => <<2#11101011101011101:17>>,
            $=  => <<2#1110101010111:13>>,
            $+  => <<2#1011101011101:13>>,
            $/  => <<2#1110101011101:13>>,
            $(  => <<2#111010111011101:15>>,
            $)  => <<2#1110101110111010111:19>>,
            $_  => <<2#10101110111010111:17>>,
            $&  => <<2#10111010101:11>>,
            $$  => <<2#10101011101010111:17>>,
            $\n => <<2#10111010111:11>>,
            $\s => <<2#0:1>>
  },
  maps:get(Char, Morse).

signal(String) -> Signal = #{ "STARTING_SIGNAL" => <<2#111010111010111:15>>,
            "END_OF_WORK"     => <<2#101010111010111:15>>,
            "SOS"             => <<2#10101011101110111010101:23>>,
            "ERROR"           => <<2#101010101010101:15>>
  },
  maps:get(String, Signal).

replace(Char) -> Replace = #{ $×  => "X",
              $′  => "'",
              $″  => "''",
              $“  => "\"",
              $”  => "\"",
              $%  => "0/0",
              $‰  => "0/00",
              $½  => "1/2",
              $⅓  => "1/3",
              $⅔  => "2/3",
              $¼  => "1/4",
              $¾  => "3/4",
              $⅕  => "1/5",
              $⅖  => "2/5",
              $⅗  => "3/5",
              $⅘  => "4/5",
              $⅙  => "1/6",
              $⅚  => "5/6",
              $⅐  => "1/7",
              $⅛  => "1/8",
              $⅜  => "3/8",
              $⅝  => "5/8",
              $⅞  => "7/8",
              $⅑  => "1/9",
              $⅒  => "1/10"
  },
  maps:get(Char, Replace).

main([File])       -> main(pretty, File);
main(["-t", File]) -> main(terms,  File).

main(Print, File) ->
    ok = io:setopts([{encoding, unicode}]),
    {ok, Content} = file:consult(File),
    Content2 = lists:map(fun(X) -> code(X) end, Content),
    lists:map(fun(X) -> print(Print, X) end, Content2).

code(<<Bits/bits>>) -> decode(Bits);
code(String)        -> encode(string:to_upper(String)).

print(pretty, <<Bits/bits>>) -> io:format("~s\n",  [[$0+X || <<X:1>> <= Bits]]);
print(pretty, String)        -> io:format("~ts\n", [String]);
print(terms,  <<Bits/bits>>) -> io:format("~w.\n", [Bits]);
print(terms,  String)        -> io:format("~p.\n", [String]).


encode(String)                            -> encode(String, <<>>).
encode([], R)                             -> <<(signal("STARTING_SIGNAL"))/bits,0:3,R/bits,(signal("END_OF_WORK"))/bits>>;
encode([X,Y|T], R)
    when X >= $1 andalso X =< $9 andalso (Y >= $¼ andalso Y =< $¾ orelse Y >= $⅐ andalso Y =< $⅞ orelse Y =:= $% orelse Y =:= $‰)
  orelse Y >= $1 andalso Y =< $9 andalso (X >= $¼ andalso X =< $¾ orelse X >= $⅐ andalso X =< $⅞) -> encode([X,$-,Y|T], R);
encode([ X|T], R)                         -> encode((replace(X)) ++ T, R);
encode([ X|T], R)                         -> encode(T, <<R/bits,(morse(X))/bits,0:3>>);
encode("<SOS>" ++ T, R)                   -> encode(T, <<R/bits,(signal("SOS"))/bits,0:3>>);
encode([ _|T], R)                         -> encode(T, <<R/bits,(signal("ERROR"))/bits,0:3>>).


decode(Bits)                                          -> decode(Bits, []).
decode(<<(signal("STARTING_SIGNAL")),0:3,T/bits>>, R) -> decode(T, R);
decode(<<(morse(X)),0:3,T/bits>>, R)                  -> decode(T, [X|R]);
decode(<<(signal("ERROR")),0:3,T/bits>>, R)           -> decode(T, [$�|R]);
decode(<<(signal("SOS")),0:3,T/bits>>, R)             -> decode(T, ">SOS<" ++ R);
decode(<<(signal("END_OF_WORK"))>>, R)                -> lists:reverse(R).
