% Copyright (C) 2019, Florent Gallaire <f@gallai.re>
% MIT license

-module(morse_map).
-export([main/1, morse/1, code/1, encode/1, decode/1]).
-compile({parse_transform, morse_map_pt}).

morse(Char) -> Morse = #{ $0 => <<2#1110111011101110111:19>>,
            $1 => <<2#10111011101110111:17>>,
            $2 => <<2#101011101110111:15>>, 
            $3 => <<2#1010101110111:13>>,
            $4 => <<2#10101010111:11>>,
            $5 => <<2#101010101:9>>,
            $6 => <<2#11101010101:11>>,
            $7 => <<2#1110111010101:13>>,
            $8 => <<2#111011101110101:15>>,
            $9 => <<2#11101110111011101:17>>,

            $A => <<2#10111:5>>,
            $À => <<2#101110111010111:15>>,
            $B => <<2#111010101:9>>,
            $C => <<2#11101011101:11>>,
            $Ç => <<2#1110101110101:13>>,
            $D => <<2#1110101:7>>,
            $E => <<2#1:1>>,
            $É => <<2#10101110101:11>>,
            $È => <<2#1011101010111:13>>,
            $F => <<2#101011101:9>>,
            $G => <<2#111011101:9>>,
            $H => <<2#1010101:7>>,
            $I => <<2#101:3>>,
            $J => <<2#1011101110111:13>>,
            $K => <<2#111010111:9>>,
            $L => <<2#101110101:9>>,
            $M => <<2#1110111:7>>,
            $N => <<2#11101:5>>,
            $O => <<2#11101110111:11>>,
            $P => <<2#10111011101:11>>,
            $Q => <<2#1110111010111:13>>,
            $R => <<2#1011101:7>>,
            $S => <<2#10101:5>>,
            $T => <<2#111:3>>,
            $U => <<2#1010111:7>>,
            $V => <<2#101010111:9>>,
            $W => <<2#101110111:9>>,
            $X => <<2#11101010111:11>>,
            $Y => <<2#1110101110111:13>>,
            $Z => <<2#11101110101:11>>,

            $.  => <<2#10111010111010111:17>>,
            $,  => <<2#1110111010101110111:19>>,
            $:  => <<2#11101110111010101:17>>,
            $?  => <<2#101011101110101:15>>,
            $'  => <<2#1011101110111011101:19>>,
            $-  => <<2#111010101010111:15>>,
            $"  => <<2#101110101011101:15>>,
            $@  => <<2#10111011101011101:17>>,
            $!  => <<2#1110101110101110111:19>>,
            $;  => <<2#11101011101011101:17>>,
            $=  => <<2#1110101010111:13>>,
            $+  => <<2#1011101011101:13>>,
            $/  => <<2#1110101011101:13>>,
            $(  => <<2#111010111011101:15>>,
            $)  => <<2#1110101110111010111:19>>,
            $_  => <<2#10101110111010111:17>>,
            $&  => <<2#10111010101:11>>,
            $$  => <<2#10101011101010111:17>>,
            $\n => <<2#10111010111:11>>,
            $\s => <<2#0:1>>,

            "STARTING_SIGNAL" => <<2#111010111010111:15>>,
            "END_OF_WORK"     => <<2#101010111010111:15>>,
            "SOS"             => <<2#10101011101110111010101:23>>,
            "ERROR"           => <<2#101010101010101:15>>
  },
  maps:get(Char, Morse).

main([File])       -> main(pretty, File);
main(["-t", File]) -> main(terms,  File).

main(Print, File) ->
    ok = io:setopts([{encoding, unicode}]),
    {ok, Content} = file:consult(File),
    Content2 = lists:map(fun(X) -> code(X) end, Content),
    lists:map(fun(X) -> print(Print, X) end, Content2).

code(<<Bits/bits>>) -> decode(Bits);
code(String)        -> encode(string:to_upper(String)).

print(pretty, <<Bits/bits>>) -> io:format("~s\n",  [[$0+X || <<X:1>> <= Bits]]);
print(pretty, String)        -> io:format("~ts\n", [String]);
print(terms,  <<Bits/bits>>) -> io:format("~w.\n", [Bits]);
print(terms,  String)        -> io:format("~p.\n", [String]).


encode(String)                            -> encode(String, <<>>).
encode([], R)                             -> <<(morse("STARTING_SIGNAL"))/bits,0:3,R/bits,(morse("END_OF_WORK"))/bits>>;
encode([X,Y|T], R)
    when X >= $1 andalso X =< $9 andalso (Y >= $¼ andalso Y =< $¾ orelse Y >= $⅐ andalso Y =< $⅞ orelse Y =:= $% orelse Y =:= $‰)
  orelse Y >= $1 andalso Y =< $9 andalso (X >= $¼ andalso X =< $¾ orelse X >= $⅐ andalso X =< $⅞) -> encode([X,$-,Y|T], R);
encode([$×|T], R)                         -> encode("X" ++ T, R);
encode([$′|T], R)                         -> encode("'" ++ T, R);
encode([$″|T], R)                         -> encode("''" ++ T, R);
encode([$%|T], R)                         -> encode("0/0" ++ T, R);
encode([$‰|T], R)                         -> encode("0/00" ++ T, R);
encode([$½|T], R)                         -> encode("1/2" ++ T, R);
encode([$⅓|T], R)                         -> encode("1/3" ++ T, R);
encode([$⅔|T], R)                         -> encode("2/3" ++ T, R);
encode([$¼|T], R)                         -> encode("1/4" ++ T, R);
encode([$¾|T], R)                         -> encode("3/4" ++ T, R);
encode([$⅕|T], R)                         -> encode("1/5" ++ T, R);
encode([$⅖|T], R)                         -> encode("2/5" ++ T, R);
encode([$⅗|T], R)                         -> encode("3/5" ++ T, R);
encode([$⅘|T], R)                         -> encode("4/5" ++ T, R);
encode([$⅙|T], R)                         -> encode("1/6" ++ T, R);
encode([$⅚|T], R)                         -> encode("5/6" ++ T, R);
encode([$⅐|T], R)                         -> encode("1/7" ++ T, R);
encode([$⅛|T], R)                         -> encode("1/8" ++ T, R);
encode([$⅜|T], R)                         -> encode("3/8" ++ T, R);
encode([$⅝|T], R)                         -> encode("5/8" ++ T, R);
encode([$⅞|T], R)                         -> encode("7/8" ++ T, R);
encode([$⅑|T], R)                         -> encode("1/9" ++ T, R);
encode([$⅒|T], R)                         -> encode("1/10" ++ T, R);
encode([ X|T], R) when X =:= $“; X =:= $” -> encode("\"" ++ T, R);
encode([$0|T], R)                         -> encode(T, <<R/bits,(morse($0))/bits,0:3>>);
encode([$1|T], R)                         -> encode(T, <<R/bits,(morse($1))/bits,0:3>>);
encode([$2|T], R)                         -> encode(T, <<R/bits,(morse($2))/bits,0:3>>);
encode([$3|T], R)                         -> encode(T, <<R/bits,(morse($3))/bits,0:3>>);
encode([$4|T], R)                         -> encode(T, <<R/bits,(morse($4))/bits,0:3>>);
encode([$5|T], R)                         -> encode(T, <<R/bits,(morse($5))/bits,0:3>>);
encode([$6|T], R)                         -> encode(T, <<R/bits,(morse($6))/bits,0:3>>);
encode([$7|T], R)                         -> encode(T, <<R/bits,(morse($7))/bits,0:3>>);
encode([$8|T], R)                         -> encode(T, <<R/bits,(morse($8))/bits,0:3>>);
encode([$9|T], R)                         -> encode(T, <<R/bits,(morse($9))/bits,0:3>>);
encode([$.|T], R)                         -> encode(T, <<R/bits,(morse($.))/bits,0:3>>);
encode([$,|T], R)                         -> encode(T, <<R/bits,(morse($,))/bits,0:3>>);
encode([$:|T], R)                         -> encode(T, <<R/bits,(morse($:))/bits,0:3>>);
encode([$?|T], R)                         -> encode(T, <<R/bits,(morse($?))/bits,0:3>>);
encode([$-|T], R)                         -> encode(T, <<R/bits,(morse($-))/bits,0:3>>);
encode([$/|T], R)                         -> encode(T, <<R/bits,(morse($/))/bits,0:3>>);
encode([$(|T], R)                         -> encode(T, <<R/bits,(morse($())/bits,0:3>>);
encode([$)|T], R)                         -> encode(T, <<R/bits,(morse($)))/bits,0:3>>);
encode([$=|T], R)                         -> encode(T, <<R/bits,(morse($=))/bits,0:3>>);
encode([$+|T], R)                         -> encode(T, <<R/bits,(morse($+))/bits,0:3>>);
encode([$@|T], R)                         -> encode(T, <<R/bits,(morse($@))/bits,0:3>>);
encode([$!|T], R)                         -> encode(T, <<R/bits,(morse($!))/bits,0:3>>);
encode([$;|T], R)                         -> encode(T, <<R/bits,(morse($;))/bits,0:3>>);
encode([$_|T], R)                         -> encode(T, <<R/bits,(morse($_))/bits,0:3>>);
encode([$&|T], R)                         -> encode(T, <<R/bits,(morse($&))/bits,0:3>>);
encode([$$|T], R)                         -> encode(T, <<R/bits,(morse($$))/bits,0:3>>);
encode([$\n|T], R)                        -> encode(T, <<R/bits,(morse($\n))/bits,0:3>>);
encode([$\s|T], R)                        -> encode(T, <<R/bits,(morse($\s))/bits,0:3>>);
encode([$'|T], R)                         -> encode(T, <<R/bits,(morse($'))/bits,0:3>>);
encode([$"|T], R)                         -> encode(T, <<R/bits,(morse($"))/bits,0:3>>);
encode([$A|T], R)                         -> encode(T, <<R/bits,(morse($A))/bits,0:3>>);
encode([$À|T], R)                         -> encode(T, <<R/bits,(morse($À))/bits,0:3>>);
encode([$B|T], R)                         -> encode(T, <<R/bits,(morse($B))/bits,0:3>>);
encode([$C|T], R)                         -> encode(T, <<R/bits,(morse($C))/bits,0:3>>);
encode([$Ç|T], R)                         -> encode(T, <<R/bits,(morse($Ç))/bits,0:3>>);
encode([$D|T], R)                         -> encode(T, <<R/bits,(morse($D))/bits,0:3>>);
encode([$E|T], R)                         -> encode(T, <<R/bits,(morse($E))/bits,0:3>>);
encode([$É|T], R)                         -> encode(T, <<R/bits,(morse($É))/bits,0:3>>);
encode([$È|T], R)                         -> encode(T, <<R/bits,(morse($È))/bits,0:3>>);
encode([$F|T], R)                         -> encode(T, <<R/bits,(morse($F))/bits,0:3>>);
encode([$G|T], R)                         -> encode(T, <<R/bits,(morse($G))/bits,0:3>>);
encode([$H|T], R)                         -> encode(T, <<R/bits,(morse($H))/bits,0:3>>);
encode([$I|T], R)                         -> encode(T, <<R/bits,(morse($I))/bits,0:3>>);
encode([$J|T], R)                         -> encode(T, <<R/bits,(morse($J))/bits,0:3>>);
encode([$K|T], R)                         -> encode(T, <<R/bits,(morse($K))/bits,0:3>>);
encode([$L|T], R)                         -> encode(T, <<R/bits,(morse($L))/bits,0:3>>);
encode([$M|T], R)                         -> encode(T, <<R/bits,(morse($M))/bits,0:3>>);
encode([$N|T], R)                         -> encode(T, <<R/bits,(morse($N))/bits,0:3>>);
encode([$O|T], R)                         -> encode(T, <<R/bits,(morse($O))/bits,0:3>>);
encode([$P|T], R)                         -> encode(T, <<R/bits,(morse($P))/bits,0:3>>);
encode([$Q|T], R)                         -> encode(T, <<R/bits,(morse($Q))/bits,0:3>>);
encode([$R|T], R)                         -> encode(T, <<R/bits,(morse($R))/bits,0:3>>);
encode([$S|T], R)                         -> encode(T, <<R/bits,(morse($S))/bits,0:3>>);
encode([$T|T], R)                         -> encode(T, <<R/bits,(morse($T))/bits,0:3>>);
encode([$U|T], R)                         -> encode(T, <<R/bits,(morse($U))/bits,0:3>>);
encode([$V|T], R)                         -> encode(T, <<R/bits,(morse($V))/bits,0:3>>);
encode([$W|T], R)                         -> encode(T, <<R/bits,(morse($W))/bits,0:3>>);
encode([$X|T], R)                         -> encode(T, <<R/bits,(morse($X))/bits,0:3>>);
encode([$Y|T], R)                         -> encode(T, <<R/bits,(morse($Y))/bits,0:3>>);
encode([$Z|T], R)                         -> encode(T, <<R/bits,(morse($Z))/bits,0:3>>);
encode("<SOS>" ++ T, R)                   -> encode(T, <<R/bits,(morse("SOS"))/bits,0:3>>);
encode([ _|T], R)                         -> encode(T, <<R/bits,(morse("ERROR"))/bits,0:3>>).


decode(Bits)                                         -> decode(Bits, []).
decode(<<(morse("STARTING_SIGNAL")),0:3,T/bits>>, R) -> decode(T, R);
decode(<<(morse($0)),0:3,T/bits>>, R)                -> decode(T, [$0|R]);
decode(<<(morse($1)),0:3,T/bits>>, R)                -> decode(T, [$1|R]);
decode(<<(morse($2)),0:3,T/bits>>, R)                -> decode(T, [$2|R]);
decode(<<(morse($3)),0:3,T/bits>>, R)                -> decode(T, [$3|R]);
decode(<<(morse($4)),0:3,T/bits>>, R)                -> decode(T, [$4|R]);
decode(<<(morse($5)),0:3,T/bits>>, R)                -> decode(T, [$5|R]);
decode(<<(morse($6)),0:3,T/bits>>, R)                -> decode(T, [$6|R]);
decode(<<(morse($7)),0:3,T/bits>>, R)                -> decode(T, [$7|R]);
decode(<<(morse($8)),0:3,T/bits>>, R)                -> decode(T, [$8|R]);
decode(<<(morse($9)),0:3,T/bits>>, R)                -> decode(T, [$9|R]);
decode(<<(morse($.)),0:3,T/bits>>, R)                -> decode(T, [$.|R]);
decode(<<(morse($,)),0:3,T/bits>>, R)                -> decode(T, [$,|R]);
decode(<<(morse($:)),0:3,T/bits>>, R)                -> decode(T, [$:|R]);
decode(<<(morse($?)),0:3,T/bits>>, R)                -> decode(T, [$?|R]);
decode(<<(morse($')),0:3,T/bits>>, R)                -> decode(T, [$'|R]);
decode(<<(morse($-)),0:3,T/bits>>, R)                -> decode(T, [$-|R]);
decode(<<(morse($/)),0:3,T/bits>>, R)                -> decode(T, [$/|R]);
decode(<<(morse($()),0:3,T/bits>>, R)                -> decode(T, [$(|R]);
decode(<<(morse($))),0:3,T/bits>>, R)                -> decode(T, [$)|R]);
decode(<<(morse($")),0:3,T/bits>>, R)                -> decode(T, [$"|R]);
decode(<<(morse($=)),0:3,T/bits>>, R)                -> decode(T, [$=|R]);
decode(<<(morse($+)),0:3,T/bits>>, R)                -> decode(T, [$+|R]);
decode(<<(morse($@)),0:3,T/bits>>, R)                -> decode(T, [$@|R]);
decode(<<(morse($!)),0:3,T/bits>>, R)                -> decode(T, [$!|R]);
decode(<<(morse($;)),0:3,T/bits>>, R)                -> decode(T, [$;|R]);
decode(<<(morse($_)),0:3,T/bits>>, R)                -> decode(T, [$_|R]);
decode(<<(morse($&)),0:3,T/bits>>, R)                -> decode(T, [$&|R]);
decode(<<(morse($$)),0:3,T/bits>>, R)                -> decode(T, [$$|R]);
decode(<<(morse($\n)),0:3,T/bits>>, R)               -> decode(T, [$\n|R]);
decode(<<(morse($\s)),0:3,T/bits>>, R)               -> decode(T, [$\s|R]);
decode(<<(morse($A)),0:3,T/bits>>, R)                -> decode(T, [$A|R]);
decode(<<(morse($À)),0:3,T/bits>>, R)                -> decode(T, [$À|R]);
decode(<<(morse($B)),0:3,T/bits>>, R)                -> decode(T, [$B|R]);
decode(<<(morse($C)),0:3,T/bits>>, R)                -> decode(T, [$C|R]);
decode(<<(morse($Ç)),0:3,T/bits>>, R)                -> decode(T, [$Ç|R]);
decode(<<(morse($D)),0:3,T/bits>>, R)                -> decode(T, [$D|R]);
decode(<<(morse($E)),0:3,T/bits>>, R)                -> decode(T, [$E|R]);
decode(<<(morse($É)),0:3,T/bits>>, R)                -> decode(T, [$É|R]);
decode(<<(morse($È)),0:3,T/bits>>, R)                -> decode(T, [$È|R]);
decode(<<(morse($F)),0:3,T/bits>>, R)                -> decode(T, [$F|R]);
decode(<<(morse($G)),0:3,T/bits>>, R)                -> decode(T, [$G|R]);
decode(<<(morse($H)),0:3,T/bits>>, R)                -> decode(T, [$H|R]);
decode(<<(morse($I)),0:3,T/bits>>, R)                -> decode(T, [$I|R]);
decode(<<(morse($J)),0:3,T/bits>>, R)                -> decode(T, [$J|R]);
decode(<<(morse($K)),0:3,T/bits>>, R)                -> decode(T, [$K|R]);
decode(<<(morse($L)),0:3,T/bits>>, R)                -> decode(T, [$L|R]);
decode(<<(morse($M)),0:3,T/bits>>, R)                -> decode(T, [$M|R]);
decode(<<(morse($N)),0:3,T/bits>>, R)                -> decode(T, [$N|R]);
decode(<<(morse($O)),0:3,T/bits>>, R)                -> decode(T, [$O|R]);
decode(<<(morse($P)),0:3,T/bits>>, R)                -> decode(T, [$P|R]);
decode(<<(morse($Q)),0:3,T/bits>>, R)                -> decode(T, [$Q|R]);
decode(<<(morse($R)),0:3,T/bits>>, R)                -> decode(T, [$R|R]);
decode(<<(morse($S)),0:3,T/bits>>, R)                -> decode(T, [$S|R]);
decode(<<(morse($T)),0:3,T/bits>>, R)                -> decode(T, [$T|R]);
decode(<<(morse($U)),0:3,T/bits>>, R)                -> decode(T, [$U|R]);
decode(<<(morse($V)),0:3,T/bits>>, R)                -> decode(T, [$V|R]);
decode(<<(morse($W)),0:3,T/bits>>, R)                -> decode(T, [$W|R]);
decode(<<(morse($X)),0:3,T/bits>>, R)                -> decode(T, [$X|R]);
decode(<<(morse($Y)),0:3,T/bits>>, R)                -> decode(T, [$Y|R]);
decode(<<(morse($Z)),0:3,T/bits>>, R)                -> decode(T, [$Z|R]);
decode(<<(morse("ERROR")),0:3,T/bits>>, R)           -> decode(T, [$�|R]);
decode(<<(morse("SOS")),0:3,T/bits>>, R)             -> decode(T, ">SOS<" ++ R);
decode(<<(morse("END_OF_WORK"))>>, R)                -> lists:reverse(R).
