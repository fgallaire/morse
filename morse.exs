#!/usr/bin/env elixir
# Copyright (C) 2019, Florent Gallaire <f@gallai.re>
# MIT license

defmodule Morse do
  @digits '0123456789'
  @fractions '½⅓⅔¼¾⅕⅖⅗⅘⅙⅚⅐⅛⅜⅝⅞⅑⅒'

  @zero <<0b1110111011101110111::19>>
  @one <<0b10111011101110111::17>>
  @two <<0b101011101110111::15>>
  @three <<0b1010101110111::13>>
  @four <<0b10101010111::11>>
  @five <<0b101010101::9>>
  @six <<0b11101010101::11>>
  @seven <<0b1110111010101::13>>
  @eight <<0b111011101110101::15>>
  @nine <<0b11101110111011101::17>>

  @a <<0b10111::5>>
  @à <<0b101110111010111::15>>
  @b <<0b111010101::9>>
  @c <<0b11101011101::11>>
  @ç <<0b1110101110101::13>>
  @d <<0b1110101::7>>
  @e <<0b1::1>>
  @é <<0b10101110101::11>>
  @è <<0b1011101010111::13>>
  @f <<0b101011101::9>>
  @g <<0b111011101::9>>
  @h <<0b1010101::7>>
  @i <<0b101::3>>
  @j <<0b1011101110111::13>>
  @k <<0b111010111::9>>
  @l <<0b101110101::9>>
  @m <<0b1110111::7>>
  @n <<0b11101::5>>
  @o <<0b11101110111::11>>
  @p <<0b10111011101::11>>
  @q <<0b1110111010111::13>>
  @r <<0b1011101::7>>
  @s <<0b10101::5>>
  @t <<0b111::3>>
  @u <<0b1010111::7>>
  @v <<0b101010111::9>>
  @w <<0b101110111::9>>
  @x <<0b11101010111::11>>
  @y <<0b1110101110111::13>>
  @z <<0b11101110101::11>>

  @period <<0b10111010111010111::17>>
  @comma <<0b1110111010101110111::19>>
  @colon <<0b11101110111010101::17>>
  @question <<0b101011101110101::15>>
  @apostrophe <<0b1011101110111011101::19>>
  @hyphen <<0b111010101010111::15>>
  @quotation <<0b101110101011101::15>>
  @at <<0b10111011101011101::17>>
  @exclamation <<0b1110101110101110111::19>>
  @semicolon <<0b11101011101011101::17>>
  @equals <<0b1110101010111::13>>
  @plus <<0b1011101011101::13>>
  @slash <<0b1110101011101::13>>
  @open <<0b111010111011101::15>>
  @close <<0b1110101110111010111::19>>
  @underscore <<0b10101110111010111::17>>
  @ampersand <<0b10111010101::11>>
  @dollar <<0b10101011101010111::17>>
  @new_line <<0b10111010111::11>>
  @space <<0b0::1>>

  @starting_signal <<0b111010111010111::15>>
  @end_of_work     <<0b101010111010111::15>>
  @sos             <<0b10101011101110111010101::23>>
  @error           <<0b101010101010101::15>>
        
  def encode(charlist),                 do: encode(charlist, <<>>)
  def encode([], r),                    do: <<@starting_signal,0::3,r::bits,@end_of_work>>
  def encode([x,y|t], r) when x in @digits and (y in @fractions or y in '%‰')
                           or y in @digits and  x in @fractions,
                                        do: encode([x,?-,y|t], r)
  def encode([?×|t], r),                do: encode('X' ++ t, r)
  def encode([?′|t], r),                do: encode('\'' ++ t, r)
  def encode([?″|t], r),                do: encode('\'\'' ++ t, r)
  def encode([?%|t], r),                do: encode('0/0' ++ t, r)
  def encode([?‰|t], r),                do: encode('0/00' ++ t, r)
  def encode([?½|t], r),                do: encode('1/2' ++ t, r)
  def encode([?⅓|t], r),                do: encode('1/3' ++ t, r)
  def encode([?⅔|t], r),                do: encode('2/3' ++ t, r)
  def encode([?¼|t], r),                do: encode('1/4' ++ t, r)
  def encode([?¾|t], r),                do: encode('3/4' ++ t, r)
  def encode([?⅕|t], r),                do: encode('1/5' ++ t, r)
  def encode([?⅖|t], r),                do: encode('2/5' ++ t, r)
  def encode([?⅗|t], r),                do: encode('3/5' ++ t, r)
  def encode([?⅘|t], r),                do: encode('4/5' ++ t, r)
  def encode([?⅙|t], r),                do: encode('1/6' ++ t, r)
  def encode([?⅚|t], r),                do: encode('5/6' ++ t, r)
  def encode([?⅐|t], r),                do: encode('1/7' ++ t, r)
  def encode([?⅛|t], r),                do: encode('1/8' ++ t, r)
  def encode([?⅜|t], r),                do: encode('3/8' ++ t, r)
  def encode([?⅝|t], r),                do: encode('5/8' ++ t, r)
  def encode([?⅞|t], r),                do: encode('7/8' ++ t, r)
  def encode([?⅑|t], r),                do: encode('1/9' ++ t, r)
  def encode([?⅒|t], r),                do: encode('1/10' ++ t, r)
  def encode([ x|t], r) when x in '“”', do: encode('"' ++ t, r)
  def encode([?0|t], r),                do: encode(t, <<r::bits,@zero,0::3>>)
  def encode([?1|t], r),                do: encode(t, <<r::bits,@one,0::3>>)
  def encode([?2|t], r),                do: encode(t, <<r::bits,@two,0::3>>)
  def encode([?3|t], r),                do: encode(t, <<r::bits,@three,0::3>>)
  def encode([?4|t], r),                do: encode(t, <<r::bits,@four,0::3>>)
  def encode([?5|t], r),                do: encode(t, <<r::bits,@five,0::3>>)
  def encode([?6|t], r),                do: encode(t, <<r::bits,@six,0::3>>)
  def encode([?7|t], r),                do: encode(t, <<r::bits,@seven,0::3>>)
  def encode([?8|t], r),                do: encode(t, <<r::bits,@eight,0::3>>)
  def encode([?9|t], r),                do: encode(t, <<r::bits,@nine,0::3>>)
  def encode([?.|t], r),                do: encode(t, <<r::bits,@period,0::3>>)
  def encode([?,|t], r),                do: encode(t, <<r::bits,@comma,0::3>>)
  def encode([?:|t], r),                do: encode(t, <<r::bits,@colon,0::3>>)
  def encode([??|t], r),                do: encode(t, <<r::bits,@question,0::3>>)
  def encode([?-|t], r),                do: encode(t, <<r::bits,@hyphen,0::3>>)
  def encode([?/|t], r),                do: encode(t, <<r::bits,@slash,0::3>>)
  def encode([?(|t], r),                do: encode(t, <<r::bits,@open,0::3>>)
  def encode([?)|t], r),                do: encode(t, <<r::bits,@close,0::3>>)
  def encode([?=|t], r),                do: encode(t, <<r::bits,@equals,0::3>>)
  def encode([?+|t], r),                do: encode(t, <<r::bits,@plus,0::3>>)
  def encode([?@|t], r),                do: encode(t, <<r::bits,@at,0::3>>)
  def encode([?!|t], r),                do: encode(t, <<r::bits,@exclamation,0::3>>)
  def encode([?;|t], r),                do: encode(t, <<r::bits,@semicolon,0::3>>)
  def encode([?_|t], r),                do: encode(t, <<r::bits,@underscore,0::3>>)
  def encode([?&|t], r),                do: encode(t, <<r::bits,@ampersand,0::3>>)
  def encode([?$|t], r),                do: encode(t, <<r::bits,@dollar,0::3>>)
  def encode([?'|t], r),                do: encode(t, <<r::bits,@apostrophe,0::3>>)
  def encode([?"|t], r),                do: encode(t, <<r::bits,@quotation,0::3>>)
  def encode([?\n|t], r),               do: encode(t, <<r::bits,@new_line,0::3>>)
  def encode([?\s|t], r),               do: encode(t, <<r::bits,@space,0::3>>)
  def encode([?A|t], r),                do: encode(t, <<r::bits,@a,0::3>>)
  def encode([?À|t], r),                do: encode(t, <<r::bits,@à,0::3>>)
  def encode([?B|t], r),                do: encode(t, <<r::bits,@b,0::3>>)
  def encode([?C|t], r),                do: encode(t, <<r::bits,@c,0::3>>)
  def encode([?Ç|t], r),                do: encode(t, <<r::bits,@ç,0::3>>)
  def encode([?D|t], r),                do: encode(t, <<r::bits,@d,0::3>>)
  def encode([?E|t], r),                do: encode(t, <<r::bits,@e,0::3>>)
  def encode([?É|t], r),                do: encode(t, <<r::bits,@é,0::3>>)
  def encode([?È|t], r),                do: encode(t, <<r::bits,@è,0::3>>)
  def encode([?F|t], r),                do: encode(t, <<r::bits,@f,0::3>>)
  def encode([?G|t], r),                do: encode(t, <<r::bits,@g,0::3>>)
  def encode([?H|t], r),                do: encode(t, <<r::bits,@h,0::3>>)
  def encode([?I|t], r),                do: encode(t, <<r::bits,@i,0::3>>)
  def encode([?J|t], r),                do: encode(t, <<r::bits,@j,0::3>>)
  def encode([?K|t], r),                do: encode(t, <<r::bits,@k,0::3>>)
  def encode([?L|t], r),                do: encode(t, <<r::bits,@l,0::3>>)
  def encode([?M|t], r),                do: encode(t, <<r::bits,@m,0::3>>)
  def encode([?N|t], r),                do: encode(t, <<r::bits,@n,0::3>>)
  def encode([?O|t], r),                do: encode(t, <<r::bits,@o,0::3>>)
  def encode([?P|t], r),                do: encode(t, <<r::bits,@p,0::3>>)
  def encode([?Q|t], r),                do: encode(t, <<r::bits,@q,0::3>>)
  def encode([?R|t], r),                do: encode(t, <<r::bits,@r,0::3>>)
  def encode([?S|t], r),                do: encode(t, <<r::bits,@s,0::3>>)
  def encode([?T|t], r),                do: encode(t, <<r::bits,@t,0::3>>)
  def encode([?U|t], r),                do: encode(t, <<r::bits,@u,0::3>>)
  def encode([?V|t], r),                do: encode(t, <<r::bits,@v,0::3>>)
  def encode([?W|t], r),                do: encode(t, <<r::bits,@w,0::3>>)
  def encode([?X|t], r),                do: encode(t, <<r::bits,@x,0::3>>)
  def encode([?Y|t], r),                do: encode(t, <<r::bits,@y,0::3>>)
  def encode([?Z|t], r),                do: encode(t, <<r::bits,@z,0::3>>)
  def encode('<SOS>' ++ t, r),          do: encode(t, <<r::bits,@sos,0::3>>)
  def encode([ _|t], r),                do: encode(t, <<r::bits,@error,0::3>>)

  def decode(bits),                                 do: decode(bits, [])
  def decode(<<@starting_signal,0::3,t::bits>>, r), do: decode(t, r)
  def decode(<<@zero,0::3,t::bits>>, r),            do: decode(t, [?0|r])
  def decode(<<@one,0::3,t::bits>>, r),             do: decode(t, [?1|r])
  def decode(<<@two,0::3,t::bits>>, r),             do: decode(t, [?2|r])
  def decode(<<@three,0::3,t::bits>>, r),           do: decode(t, [?3|r])
  def decode(<<@four,0::3,t::bits>>, r),            do: decode(t, [?4|r])
  def decode(<<@five,0::3,t::bits>>, r),            do: decode(t, [?5|r])
  def decode(<<@six,0::3,t::bits>>, r),             do: decode(t, [?6|r])
  def decode(<<@seven,0::3,t::bits>>, r),           do: decode(t, [?7|r])
  def decode(<<@eight,0::3,t::bits>>, r),           do: decode(t, [?8|r])
  def decode(<<@nine,0::3,t::bits>>, r),            do: decode(t, [?9|r])
  def decode(<<@period,0::3,t::bits>>, r),          do: decode(t, [?.|r])
  def decode(<<@comma,0::3,t::bits>>, r),           do: decode(t, [?,|r])
  def decode(<<@colon,0::3,t::bits>>, r),           do: decode(t, [?:|r])
  def decode(<<@question,0::3,t::bits>>, r),        do: decode(t, [??|r])
  def decode(<<@apostrophe,0::3,t::bits>>, r),      do: decode(t, [?'|r])
  def decode(<<@hyphen,0::3,t::bits>>, r),          do: decode(t, [?-|r])
  def decode(<<@slash,0::3,t::bits>>, r),           do: decode(t, [?/|r])
  def decode(<<@open,0::3,t::bits>>, r),            do: decode(t, [?(|r])
  def decode(<<@close,0::3,t::bits>>, r),           do: decode(t, [?)|r])
  def decode(<<@quotation,0::3,t::bits>>, r),       do: decode(t, [?"|r])
  def decode(<<@equals,0::3,t::bits>>, r),          do: decode(t, [?=|r])
  def decode(<<@plus,0::3,t::bits>>, r),            do: decode(t, [?+|r])
  def decode(<<@at,0::3,t::bits>>, r),              do: decode(t, [?@|r])
  def decode(<<@exclamation,0::3,t::bits>>, r),     do: decode(t, [?!|r])
  def decode(<<@semicolon,0::3,t::bits>>, r),       do: decode(t, [?;|r])
  def decode(<<@underscore,0::3,t::bits>>, r),      do: decode(t, [?_|r])
  def decode(<<@ampersand,0::3,t::bits>>, r),       do: decode(t, [?&|r])
  def decode(<<@dollar,0::3,t::bits>>, r),          do: decode(t, [?$|r])
  def decode(<<@a,0::3,t::bits>>, r),               do: decode(t, [?A|r])
  def decode(<<@à,0::3,t::bits>>, r),               do: decode(t, [?À|r])
  def decode(<<@b,0::3,t::bits>>, r),               do: decode(t, [?B|r])
  def decode(<<@c,0::3,t::bits>>, r),               do: decode(t, [?C|r])
  def decode(<<@ç,0::3,t::bits>>, r),               do: decode(t, [?Ç|r])
  def decode(<<@d,0::3,t::bits>>, r),               do: decode(t, [?D|r])
  def decode(<<@e,0::3,t::bits>>, r),               do: decode(t, [?E|r])
  def decode(<<@é,0::3,t::bits>>, r),               do: decode(t, [?É|r])
  def decode(<<@è,0::3,t::bits>>, r),               do: decode(t, [?È|r])
  def decode(<<@f,0::3,t::bits>>, r),               do: decode(t, [?F|r])
  def decode(<<@g,0::3,t::bits>>, r),               do: decode(t, [?G|r])
  def decode(<<@h,0::3,t::bits>>, r),               do: decode(t, [?H|r])
  def decode(<<@i,0::3,t::bits>>, r),               do: decode(t, [?I|r])
  def decode(<<@j,0::3,t::bits>>, r),               do: decode(t, [?J|r])
  def decode(<<@k,0::3,t::bits>>, r),               do: decode(t, [?K|r])
  def decode(<<@l,0::3,t::bits>>, r),               do: decode(t, [?L|r])
  def decode(<<@m,0::3,t::bits>>, r),               do: decode(t, [?M|r])
  def decode(<<@n,0::3,t::bits>>, r),               do: decode(t, [?N|r])
  def decode(<<@o,0::3,t::bits>>, r),               do: decode(t, [?O|r])
  def decode(<<@p,0::3,t::bits>>, r),               do: decode(t, [?P|r])
  def decode(<<@q,0::3,t::bits>>, r),               do: decode(t, [?Q|r])
  def decode(<<@r,0::3,t::bits>>, r),               do: decode(t, [?R|r])
  def decode(<<@s,0::3,t::bits>>, r),               do: decode(t, [?S|r])
  def decode(<<@t,0::3,t::bits>>, r),               do: decode(t, [?T|r])
  def decode(<<@u,0::3,t::bits>>, r),               do: decode(t, [?U|r])
  def decode(<<@v,0::3,t::bits>>, r),               do: decode(t, [?V|r])
  def decode(<<@w,0::3,t::bits>>, r),               do: decode(t, [?W|r])
  def decode(<<@x,0::3,t::bits>>, r),               do: decode(t, [?X|r])
  def decode(<<@y,0::3,t::bits>>, r),               do: decode(t, [?Y|r])
  def decode(<<@z,0::3,t::bits>>, r),               do: decode(t, [?Z|r])
  def decode(<<@new_line,0::3,t::bits>>, r),        do: decode(t, [?\n|r])
  def decode(<<@space,0::3,t::bits>>, r),           do: decode(t, [?\s|r])
  def decode(<<@error,0::3,t::bits>>, r),           do: decode(t, [?�|r])
  def decode(<<@sos,0::3,t::bits>>, r),             do: decode(t, '>SOS<' ++ r)
  def decode(<<@end_of_work>>, r),                  do: Enum.reverse(r)    
end

printf = fn
  :pretty, <<bits::bits>> -> :io.format("~s\n",  [(for <<x::1 <- bits>>, do: ?0+x)])
  :pretty, charlist       -> :io.format("~ts\n", [charlist])
  :terms,  <<bits::bits>> -> :io.format("~w.\n", [bits])
  :terms,  charlist       -> :io.format("~p.\n", [charlist])
end

code = fn
  <<bits::bits>> -> Morse.decode(bits)
  charlist       -> Morse.encode(:string.to_upper(charlist))
end

main2 = fn(print, file) ->
  {:ok, content} = :file.consult(file)
  content
  |> Stream.map(fn(x) -> code.(x) end)
  |>   Enum.map(fn(x) -> printf.(print, x) end)
end

main1 = fn
  [file]       -> main2.(:pretty, file)
  ["-t", file] -> main2.(:terms, file) 
end

main1.(System.argv)
