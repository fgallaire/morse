#!/usr/bin/env escript
% Copyright (C) 2019, Florent Gallaire <f@gallai.re>
% MIT license

-define(ZERO,  2#1110111011101110111:19).
-define(ONE,   2#10111011101110111:17).
-define(TWO,   2#101011101110111:15).
-define(THREE, 2#1010101110111:13).
-define(FOUR,  2#10101010111:11).
-define(FIVE,  2#101010101:9).
-define(SIX,   2#11101010101:11).
-define(SEVEN, 2#1110111010101:13).
-define(EIGHT, 2#111011101110101:15).
-define(NINE,  2#11101110111011101:17).

-define(A, 2#10111:5).
-define(À, 2#101110111010111:15).
-define(B, 2#111010101:9).
-define(C, 2#11101011101:11).
-define(Ç, 2#1110101110101:13).
-define(D, 2#1110101:7).
-define(E, 2#1:1).
-define(É, 2#10101110101:11).
-define(È, 2#1011101010111:13).
-define(F, 2#101011101:9).
-define(G, 2#111011101:9).
-define(H, 2#1010101:7).
-define(I, 2#101:3).
-define(J, 2#1011101110111:13).
-define(K, 2#111010111:9).
-define(L, 2#101110101:9).
-define(M, 2#1110111:7).
-define(N, 2#11101:5).
-define(O, 2#11101110111:11).
-define(P, 2#10111011101:11).
-define(Q, 2#1110111010111:13).
-define(R, 2#1011101:7).
-define(S, 2#10101:5).
-define(T, 2#111:3).
-define(U, 2#1010111:7).
-define(V, 2#101010111:9).
-define(W, 2#101110111:9).
-define(X, 2#11101010111:11).
-define(Y, 2#1110101110111:13).
-define(Z, 2#11101110101:11).

-define(PERIOD,      2#10111010111010111:17).
-define(COMMA,       2#1110111010101110111:19).
-define(COLON,       2#11101110111010101:17).
-define(QUESTION,    2#101011101110101:15).
-define(APOSTROPHE,  2#1011101110111011101:19).
-define(HYPHEN,      2#111010101010111:15).
-define(QUOTATION,   2#101110101011101:15).
-define(AT,          2#10111011101011101:17).
-define(EXCLAMATION, 2#1110101110101110111:19).
-define(SEMICOLON,   2#11101011101011101:17).
-define(EQUALS,      2#1110101010111:13).
-define(PLUS,        2#1011101011101:13).
-define(SLASH,       2#1110101011101:13).
-define(OPEN,        2#111010111011101:15).
-define(CLOSE,       2#1110101110111010111:19).
-define(UNDERSCORE,  2#10101110111010111:17).
-define(AMPERSAND,   2#10111010101:11).
-define(DOLLAR,      2#10101011101010111:17).
-define(NEW_LINE,    2#10111010111:11).
-define(SPACE,       2#0:1).

-define(STARTING_SIGNAL, 2#111010111010111:15).
-define(END_OF_WORK,     2#101010111010111:15).
-define(SOS,             2#10101011101110111010101:23).
-define(ERROR,           2#101010101010101:15).

main([File])       -> main(pretty, File);
main(["-t", File]) -> main(terms,  File).

main(Print, File) ->
    ok = io:setopts([{encoding, unicode}]),
    {ok, Content} = file:consult(File),
    Content2 = lists:map(fun(X) -> code(X) end, Content),
    lists:map(fun(X) -> print(Print, X) end, Content2).

code(<<Bits/bits>>) -> decode(Bits);
code(String)        -> encode(string:to_upper(String)).

print(pretty, <<Bits/bits>>) -> io:format("~s\n",  [[$0+X || <<X:1>> <= Bits]]);
print(pretty, String)        -> io:format("~ts\n", [String]);
print(terms,  <<Bits/bits>>) -> io:format("~w.\n", [Bits]);
print(terms,  String)        -> io:format("~p.\n", [String]).


encode(String)                            -> encode(String, <<>>).
encode([], R)                             -> <<?STARTING_SIGNAL,0:3,R/bits,?END_OF_WORK>>;
encode([X,Y|T], R)
    when X >= $1 andalso X =< $9 andalso (Y >= $¼ andalso Y =< $¾ orelse Y >= $⅐ andalso Y =< $⅞ orelse Y =:= $% orelse Y =:= $‰)
  orelse Y >= $1 andalso Y =< $9 andalso (X >= $¼ andalso X =< $¾ orelse X >= $⅐ andalso X =< $⅞) -> encode([X,$-,Y|T], R);
encode([$×|T], R)                         -> encode("X" ++ T, R);
encode([$′|T], R)                         -> encode("'" ++ T, R);
encode([$″|T], R)                         -> encode("''" ++ T, R);
encode([$%|T], R)                         -> encode("0/0" ++ T, R);
encode([$‰|T], R)                         -> encode("0/00" ++ T, R);
encode([$½|T], R)                         -> encode("1/2" ++ T, R);
encode([$⅓|T], R)                         -> encode("1/3" ++ T, R);
encode([$⅔|T], R)                         -> encode("2/3" ++ T, R);
encode([$¼|T], R)                         -> encode("1/4" ++ T, R);
encode([$¾|T], R)                         -> encode("3/4" ++ T, R);
encode([$⅕|T], R)                         -> encode("1/5" ++ T, R);
encode([$⅖|T], R)                         -> encode("2/5" ++ T, R);
encode([$⅗|T], R)                         -> encode("3/5" ++ T, R);
encode([$⅘|T], R)                         -> encode("4/5" ++ T, R);
encode([$⅙|T], R)                         -> encode("1/6" ++ T, R);
encode([$⅚|T], R)                         -> encode("5/6" ++ T, R);
encode([$⅐|T], R)                         -> encode("1/7" ++ T, R);
encode([$⅛|T], R)                         -> encode("1/8" ++ T, R);
encode([$⅜|T], R)                         -> encode("3/8" ++ T, R);
encode([$⅝|T], R)                         -> encode("5/8" ++ T, R);
encode([$⅞|T], R)                         -> encode("7/8" ++ T, R);
encode([$⅑|T], R)                         -> encode("1/9" ++ T, R);
encode([$⅒|T], R)                         -> encode("1/10" ++ T, R);
encode([ X|T], R) when X =:= $“; X =:= $” -> encode("\"" ++ T, R);
encode([$0|T], R)                         -> encode(T, <<R/bits,?ZERO,0:3>>);
encode([$1|T], R)                         -> encode(T, <<R/bits,?ONE,0:3>>);
encode([$2|T], R)                         -> encode(T, <<R/bits,?TWO,0:3>>);
encode([$3|T], R)                         -> encode(T, <<R/bits,?THREE,0:3>>);
encode([$4|T], R)                         -> encode(T, <<R/bits,?FOUR,0:3>>);
encode([$5|T], R)                         -> encode(T, <<R/bits,?FIVE,0:3>>);
encode([$6|T], R)                         -> encode(T, <<R/bits,?SIX,0:3>>);
encode([$7|T], R)                         -> encode(T, <<R/bits,?SEVEN,0:3>>);
encode([$8|T], R)                         -> encode(T, <<R/bits,?EIGHT,0:3>>);
encode([$9|T], R)                         -> encode(T, <<R/bits,?NINE,0:3>>);
encode([$.|T], R)                         -> encode(T, <<R/bits,?PERIOD,0:3>>);
encode([$,|T], R)                         -> encode(T, <<R/bits,?COMMA,0:3>>);
encode([$:|T], R)                         -> encode(T, <<R/bits,?COLON,0:3>>);
encode([$?|T], R)                         -> encode(T, <<R/bits,?QUESTION,0:3>>);
encode([$-|T], R)                         -> encode(T, <<R/bits,?HYPHEN,0:3>>);
encode([$/|T], R)                         -> encode(T, <<R/bits,?SLASH,0:3>>);
encode([$(|T], R)                         -> encode(T, <<R/bits,?OPEN,0:3>>);
encode([$)|T], R)                         -> encode(T, <<R/bits,?CLOSE,0:3>>);
encode([$=|T], R)                         -> encode(T, <<R/bits,?EQUALS,0:3>>);
encode([$+|T], R)                         -> encode(T, <<R/bits,?PLUS,0:3>>);
encode([$@|T], R)                         -> encode(T, <<R/bits,?AT,0:3>>);
encode([$!|T], R)                         -> encode(T, <<R/bits,?EXCLAMATION,0:3>>);
encode([$;|T], R)                         -> encode(T, <<R/bits,?SEMICOLON,0:3>>);
encode([$_|T], R)                         -> encode(T, <<R/bits,?UNDERSCORE,0:3>>);
encode([$&|T], R)                         -> encode(T, <<R/bits,?AMPERSAND,0:3>>);
encode([$$|T], R)                         -> encode(T, <<R/bits,?DOLLAR,0:3>>);
encode([$\n|T], R)                        -> encode(T, <<R/bits,?NEW_LINE,0:3>>);
encode([$\s|T], R)                        -> encode(T, <<R/bits,?SPACE,0:3>>);
encode([$'|T], R)                         -> encode(T, <<R/bits,?APOSTROPHE,0:3>>);
encode([$"|T], R)                         -> encode(T, <<R/bits,?QUOTATION,0:3>>);
encode([$A|T], R)                         -> encode(T, <<R/bits,?A,0:3>>);
encode([$À|T], R)                         -> encode(T, <<R/bits,?À,0:3>>);
encode([$B|T], R)                         -> encode(T, <<R/bits,?B,0:3>>);
encode([$C|T], R)                         -> encode(T, <<R/bits,?C,0:3>>);
encode([$Ç|T], R)                         -> encode(T, <<R/bits,?Ç,0:3>>);
encode([$D|T], R)                         -> encode(T, <<R/bits,?D,0:3>>);
encode([$E|T], R)                         -> encode(T, <<R/bits,?E,0:3>>);
encode([$É|T], R)                         -> encode(T, <<R/bits,?É,0:3>>);
encode([$È|T], R)                         -> encode(T, <<R/bits,?È,0:3>>);
encode([$F|T], R)                         -> encode(T, <<R/bits,?F,0:3>>);
encode([$G|T], R)                         -> encode(T, <<R/bits,?G,0:3>>);
encode([$H|T], R)                         -> encode(T, <<R/bits,?H,0:3>>);
encode([$I|T], R)                         -> encode(T, <<R/bits,?I,0:3>>);
encode([$J|T], R)                         -> encode(T, <<R/bits,?J,0:3>>);
encode([$K|T], R)                         -> encode(T, <<R/bits,?K,0:3>>);
encode([$L|T], R)                         -> encode(T, <<R/bits,?L,0:3>>);
encode([$M|T], R)                         -> encode(T, <<R/bits,?M,0:3>>);
encode([$N|T], R)                         -> encode(T, <<R/bits,?N,0:3>>);
encode([$O|T], R)                         -> encode(T, <<R/bits,?O,0:3>>);
encode([$P|T], R)                         -> encode(T, <<R/bits,?P,0:3>>);
encode([$Q|T], R)                         -> encode(T, <<R/bits,?Q,0:3>>);
encode([$R|T], R)                         -> encode(T, <<R/bits,?R,0:3>>);
encode([$S|T], R)                         -> encode(T, <<R/bits,?S,0:3>>);
encode([$T|T], R)                         -> encode(T, <<R/bits,?T,0:3>>);
encode([$U|T], R)                         -> encode(T, <<R/bits,?U,0:3>>);
encode([$V|T], R)                         -> encode(T, <<R/bits,?V,0:3>>);
encode([$W|T], R)                         -> encode(T, <<R/bits,?W,0:3>>);
encode([$X|T], R)                         -> encode(T, <<R/bits,?X,0:3>>);
encode([$Y|T], R)                         -> encode(T, <<R/bits,?Y,0:3>>);
encode([$Z|T], R)                         -> encode(T, <<R/bits,?Z,0:3>>);
encode("<SOS>" ++ T, R)                   -> encode(T, <<R/bits,?SOS,0:3>>);
encode([ _|T], R)                         -> encode(T, <<R/bits,?ERROR,0:3>>).


decode(Bits)                               -> decode(Bits, []).
decode(<<?STARTING_SIGNAL,0:3,T/bits>>, R) -> decode(T, R);
decode(<<?ZERO,0:3,T/bits>>, R)            -> decode(T, [$0|R]);
decode(<<?ONE,0:3,T/bits>>, R)             -> decode(T, [$1|R]);
decode(<<?TWO,0:3,T/bits>>, R)             -> decode(T, [$2|R]);
decode(<<?THREE,0:3,T/bits>>, R)           -> decode(T, [$3|R]);
decode(<<?FOUR,0:3,T/bits>>, R)            -> decode(T, [$4|R]);
decode(<<?FIVE,0:3,T/bits>>, R)            -> decode(T, [$5|R]);
decode(<<?SIX,0:3,T/bits>>, R)             -> decode(T, [$6|R]);
decode(<<?SEVEN,0:3,T/bits>>, R)           -> decode(T, [$7|R]);
decode(<<?EIGHT,0:3,T/bits>>, R)           -> decode(T, [$8|R]);
decode(<<?NINE,0:3,T/bits>>, R)            -> decode(T, [$9|R]);
decode(<<?PERIOD,0:3,T/bits>>, R)          -> decode(T, [$.|R]);
decode(<<?COMMA,0:3,T/bits>>, R)           -> decode(T, [$,|R]);
decode(<<?COLON,0:3,T/bits>>, R)           -> decode(T, [$:|R]);
decode(<<?QUESTION,0:3,T/bits>>, R)        -> decode(T, [$?|R]);
decode(<<?APOSTROPHE,0:3,T/bits>>, R)      -> decode(T, [$'|R]);
decode(<<?HYPHEN,0:3,T/bits>>, R)          -> decode(T, [$-|R]);
decode(<<?SLASH,0:3,T/bits>>, R)           -> decode(T, [$/|R]);
decode(<<?OPEN,0:3,T/bits>>, R)            -> decode(T, [$(|R]);
decode(<<?CLOSE,0:3,T/bits>>, R)           -> decode(T, [$)|R]);
decode(<<?QUOTATION,0:3,T/bits>>, R)       -> decode(T, [$"|R]);
decode(<<?EQUALS,0:3,T/bits>>, R)          -> decode(T, [$=|R]);
decode(<<?PLUS,0:3,T/bits>>, R)            -> decode(T, [$+|R]);
decode(<<?AT,0:3,T/bits>>, R)              -> decode(T, [$@|R]);
decode(<<?EXCLAMATION,0:3,T/bits>>, R)     -> decode(T, [$!|R]);
decode(<<?SEMICOLON,0:3,T/bits>>, R)       -> decode(T, [$;|R]);
decode(<<?UNDERSCORE,0:3,T/bits>>, R)      -> decode(T, [$_|R]);
decode(<<?AMPERSAND,0:3,T/bits>>, R)       -> decode(T, [$&|R]);
decode(<<?DOLLAR,0:3,T/bits>>, R)          -> decode(T, [$$|R]);
decode(<<?NEW_LINE,0:3,T/bits>>, R)        -> decode(T, [$\n|R]);
decode(<<?SPACE,0:3,T/bits>>, R)           -> decode(T, [$\s|R]);
decode(<<?A,0:3,T/bits>>, R)               -> decode(T, [$A|R]);
decode(<<?À,0:3,T/bits>>, R)               -> decode(T, [$À|R]);
decode(<<?B,0:3,T/bits>>, R)               -> decode(T, [$B|R]);
decode(<<?C,0:3,T/bits>>, R)               -> decode(T, [$C|R]);
decode(<<?Ç,0:3,T/bits>>, R)               -> decode(T, [$Ç|R]);
decode(<<?D,0:3,T/bits>>, R)               -> decode(T, [$D|R]);
decode(<<?E,0:3,T/bits>>, R)               -> decode(T, [$E|R]);
decode(<<?É,0:3,T/bits>>, R)               -> decode(T, [$É|R]);
decode(<<?È,0:3,T/bits>>, R)               -> decode(T, [$È|R]);
decode(<<?F,0:3,T/bits>>, R)               -> decode(T, [$F|R]);
decode(<<?G,0:3,T/bits>>, R)               -> decode(T, [$G|R]);
decode(<<?H,0:3,T/bits>>, R)               -> decode(T, [$H|R]);
decode(<<?I,0:3,T/bits>>, R)               -> decode(T, [$I|R]);
decode(<<?J,0:3,T/bits>>, R)               -> decode(T, [$J|R]);
decode(<<?K,0:3,T/bits>>, R)               -> decode(T, [$K|R]);
decode(<<?L,0:3,T/bits>>, R)               -> decode(T, [$L|R]);
decode(<<?M,0:3,T/bits>>, R)               -> decode(T, [$M|R]);
decode(<<?N,0:3,T/bits>>, R)               -> decode(T, [$N|R]);
decode(<<?O,0:3,T/bits>>, R)               -> decode(T, [$O|R]);
decode(<<?P,0:3,T/bits>>, R)               -> decode(T, [$P|R]);
decode(<<?Q,0:3,T/bits>>, R)               -> decode(T, [$Q|R]);
decode(<<?R,0:3,T/bits>>, R)               -> decode(T, [$R|R]);
decode(<<?S,0:3,T/bits>>, R)               -> decode(T, [$S|R]);
decode(<<?T,0:3,T/bits>>, R)               -> decode(T, [$T|R]);
decode(<<?U,0:3,T/bits>>, R)               -> decode(T, [$U|R]);
decode(<<?V,0:3,T/bits>>, R)               -> decode(T, [$V|R]);
decode(<<?W,0:3,T/bits>>, R)               -> decode(T, [$W|R]);
decode(<<?X,0:3,T/bits>>, R)               -> decode(T, [$X|R]);
decode(<<?Y,0:3,T/bits>>, R)               -> decode(T, [$Y|R]);
decode(<<?Z,0:3,T/bits>>, R)               -> decode(T, [$Z|R]);
decode(<<?ERROR,0:3,T/bits>>, R)           -> decode(T, [$�|R]);
decode(<<?SOS,0:3,T/bits>>, R)             -> decode(T, ">SOS<" ++ R);
decode(<<?END_OF_WORK>>, R)                -> lists:reverse(R).
