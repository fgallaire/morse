#!/usr/bin/env elixir
# Copyright (C) 2019, Florent Gallaire <f@gallai.re>
# MIT license

defmodule Shortcut do
  def e(x), do: Macro.escape(x)
end

defmodule Morse do
  @digits '0123456789'
  @fractions '½⅓⅔¼¾⅕⅖⅗⅘⅙⅚⅐⅛⅜⅝⅞⅑⅒'
  @morse %{ ?0 => <<0b1110111011101110111::19>>,
            ?1 => <<0b10111011101110111::17>>,
            ?2 => <<0b101011101110111::15>>, 
            ?3 => <<0b1010101110111::13>>,
            ?4 => <<0b10101010111::11>>,
            ?5 => <<0b101010101::9>>,
            ?6 => <<0b11101010101::11>>,
            ?7 => <<0b1110111010101::13>>,
            ?8 => <<0b111011101110101::15>>,
            ?9 => <<0b11101110111011101::17>>,

            ?A => <<0b10111::5>>,
            ?À => <<0b101110111010111::15>>,
            ?B => <<0b111010101::9>>,
            ?C => <<0b11101011101::11>>,
            ?Ç => <<0b1110101110101::13>>,
            ?D => <<0b1110101::7>>,
            ?E => <<0b1::1>>,
            ?É => <<0b10101110101::11>>,
            ?È => <<0b1011101010111::13>>,
            ?F => <<0b101011101::9>>,
            ?G => <<0b111011101::9>>,
            ?H => <<0b1010101::7>>,
            ?I => <<0b101::3>>,
            ?J => <<0b1011101110111::13>>,
            ?K => <<0b111010111::9>>,
            ?L => <<0b101110101::9>>,
            ?M => <<0b1110111::7>>,
            ?N => <<0b11101::5>>,
            ?O => <<0b11101110111::11>>,
            ?P => <<0b10111011101::11>>,
            ?Q => <<0b1110111010111::13>>,
            ?R => <<0b1011101::7>>,
            ?S => <<0b10101::5>>,
            ?T => <<0b111::3>>,
            ?U => <<0b1010111::7>>,
            ?V => <<0b101010111::9>>,
            ?W => <<0b101110111::9>>,
            ?X => <<0b11101010111::11>>,
            ?Y => <<0b1110101110111::13>>,
            ?Z => <<0b11101110101::11>>,

            ?.  => <<0b10111010111010111::17>>,
            ?,  => <<0b1110111010101110111::19>>,
            ?:  => <<0b11101110111010101::17>>,
            ??  => <<0b101011101110101::15>>,
            ?'  => <<0b1011101110111011101::19>>,
            ?-  => <<0b111010101010111::15>>,
            ?"  => <<0b101110101011101::15>>,
            ?@  => <<0b10111011101011101::17>>,
            ?!  => <<0b1110101110101110111::19>>,
            ?;  => <<0b11101011101011101::17>>,
            ?=  => <<0b1110101010111::13>>,
            ?+  => <<0b1011101011101::13>>,
            ?/  => <<0b1110101011101::13>>,
            ?(  => <<0b111010111011101::15>>,
            ?)  => <<0b1110101110111010111::19>>,
            ?_  => <<0b10101110111010111::17>>,
            ?&  => <<0b10111010101::11>>,
            ?$  => <<0b10101011101010111::17>>,
            ?\n => <<0b10111010111::11>>,
            ?\s => <<0b0::1>>,

            "STARTING_SIGNAL" => <<0b111010111010111::15>>,
            "END_OF_WORK"     => <<0b101010111010111::15>>,
            "SOS"             => <<0b10101011101110111010101::23>>,
            "ERROR"           => <<0b101010101010101::15>>
  }

  def encode(charlist),                 do: encode(charlist, <<>>)
  def encode([], r),                    do: <<@morse["STARTING_SIGNAL"]::bits,0::3,r::bits,@morse["END_OF_WORK"]::bits>>
  def encode([x,y|t], r) when x in @digits and (y in @fractions or y in '%‰')
                           or y in @digits and  x in @fractions,
                                        do: encode([x,?-,y|t], r)
  def encode([?×|t], r),                do: encode('X' ++ t, r)
  def encode([?′|t], r),                do: encode('\'' ++ t, r)
  def encode([?″|t], r),                do: encode('\'\'' ++ t, r)
  def encode([?%|t], r),                do: encode('0/0' ++ t, r)
  def encode([?‰|t], r),                do: encode('0/00' ++ t, r)
  def encode([?½|t], r),                do: encode('1/2' ++ t, r)
  def encode([?⅓|t], r),                do: encode('1/3' ++ t, r)
  def encode([?⅔|t], r),                do: encode('2/3' ++ t, r)
  def encode([?¼|t], r),                do: encode('1/4' ++ t, r)
  def encode([?¾|t], r),                do: encode('3/4' ++ t, r)
  def encode([?⅕|t], r),                do: encode('1/5' ++ t, r)
  def encode([?⅖|t], r),                do: encode('2/5' ++ t, r)
  def encode([?⅗|t], r),                do: encode('3/5' ++ t, r)
  def encode([?⅘|t], r),                do: encode('4/5' ++ t, r)
  def encode([?⅙|t], r),                do: encode('1/6' ++ t, r)
  def encode([?⅚|t], r),                do: encode('5/6' ++ t, r)
  def encode([?⅐|t], r),                do: encode('1/7' ++ t, r)
  def encode([?⅛|t], r),                do: encode('1/8' ++ t, r)
  def encode([?⅜|t], r),                do: encode('3/8' ++ t, r)
  def encode([?⅝|t], r),                do: encode('5/8' ++ t, r)
  def encode([?⅞|t], r),                do: encode('7/8' ++ t, r)
  def encode([?⅑|t], r),                do: encode('1/9' ++ t, r)
  def encode([?⅒|t], r),                do: encode('1/10' ++ t, r)
  def encode([ x|t], r) when x in '“”', do: encode('"' ++ t, r)
  def encode([?0|t], r),                do: encode(t, <<r::bits,@morse[?0]::bits,0::3>>)
  def encode([?1|t], r),                do: encode(t, <<r::bits,@morse[?1]::bits,0::3>>)
  def encode([?2|t], r),                do: encode(t, <<r::bits,@morse[?2]::bits,0::3>>)
  def encode([?3|t], r),                do: encode(t, <<r::bits,@morse[?3]::bits,0::3>>)
  def encode([?4|t], r),                do: encode(t, <<r::bits,@morse[?4]::bits,0::3>>)
  def encode([?5|t], r),                do: encode(t, <<r::bits,@morse[?5]::bits,0::3>>)
  def encode([?6|t], r),                do: encode(t, <<r::bits,@morse[?6]::bits,0::3>>)
  def encode([?7|t], r),                do: encode(t, <<r::bits,@morse[?7]::bits,0::3>>)
  def encode([?8|t], r),                do: encode(t, <<r::bits,@morse[?8]::bits,0::3>>)
  def encode([?9|t], r),                do: encode(t, <<r::bits,@morse[?9]::bits,0::3>>)
  def encode([?.|t], r),                do: encode(t, <<r::bits,@morse[?.]::bits,0::3>>)
  def encode([?,|t], r),                do: encode(t, <<r::bits,@morse[?,]::bits,0::3>>)
  def encode([?:|t], r),                do: encode(t, <<r::bits,@morse[?:]::bits,0::3>>)
  def encode([??|t], r),                do: encode(t, <<r::bits,@morse[??]::bits,0::3>>)
  def encode([?-|t], r),                do: encode(t, <<r::bits,@morse[?-]::bits,0::3>>)
  def encode([?/|t], r),                do: encode(t, <<r::bits,@morse[?/]::bits,0::3>>)
  def encode([?(|t], r),                do: encode(t, <<r::bits,@morse[?(]::bits,0::3>>)
  def encode([?)|t], r),                do: encode(t, <<r::bits,@morse[?)]::bits,0::3>>)
  def encode([?=|t], r),                do: encode(t, <<r::bits,@morse[?=]::bits,0::3>>)
  def encode([?+|t], r),                do: encode(t, <<r::bits,@morse[?+]::bits,0::3>>)
  def encode([?@|t], r),                do: encode(t, <<r::bits,@morse[?@]::bits,0::3>>)
  def encode([?!|t], r),                do: encode(t, <<r::bits,@morse[?!]::bits,0::3>>)
  def encode([?;|t], r),                do: encode(t, <<r::bits,@morse[?;]::bits,0::3>>)
  def encode([?_|t], r),                do: encode(t, <<r::bits,@morse[?_]::bits,0::3>>)
  def encode([?&|t], r),                do: encode(t, <<r::bits,@morse[?&]::bits,0::3>>)
  def encode([?$|t], r),                do: encode(t, <<r::bits,@morse[?$]::bits,0::3>>)
  def encode([?'|t], r),                do: encode(t, <<r::bits,@morse[?']::bits,0::3>>)
  def encode([?"|t], r),                do: encode(t, <<r::bits,@morse[?"]::bits,0::3>>)
  def encode([?\s|t], r),               do: encode(t, <<r::bits,@morse[?\s]::bits,0::3>>)
  def encode([?\n|t], r),               do: encode(t, <<r::bits,@morse[?\n]::bits,0::3>>)
  def encode([?A|t], r),                do: encode(t, <<r::bits,@morse[?A]::bits,0::3>>)
  def encode([?À|t], r),                do: encode(t, <<r::bits,@morse[?À]::bits,0::3>>)
  def encode([?B|t], r),                do: encode(t, <<r::bits,@morse[?B]::bits,0::3>>)
  def encode([?C|t], r),                do: encode(t, <<r::bits,@morse[?C]::bits,0::3>>)
  def encode([?Ç|t], r),                do: encode(t, <<r::bits,@morse[?Ç]::bits,0::3>>)
  def encode([?D|t], r),                do: encode(t, <<r::bits,@morse[?D]::bits,0::3>>)
  def encode([?E|t], r),                do: encode(t, <<r::bits,@morse[?E]::bits,0::3>>)
  def encode([?É|t], r),                do: encode(t, <<r::bits,@morse[?É]::bits,0::3>>)
  def encode([?È|t], r),                do: encode(t, <<r::bits,@morse[?È]::bits,0::3>>)
  def encode([?F|t], r),                do: encode(t, <<r::bits,@morse[?F]::bits,0::3>>)
  def encode([?G|t], r),                do: encode(t, <<r::bits,@morse[?G]::bits,0::3>>)
  def encode([?H|t], r),                do: encode(t, <<r::bits,@morse[?H]::bits,0::3>>)
  def encode([?I|t], r),                do: encode(t, <<r::bits,@morse[?I]::bits,0::3>>)
  def encode([?J|t], r),                do: encode(t, <<r::bits,@morse[?J]::bits,0::3>>)
  def encode([?K|t], r),                do: encode(t, <<r::bits,@morse[?K]::bits,0::3>>)
  def encode([?L|t], r),                do: encode(t, <<r::bits,@morse[?L]::bits,0::3>>)
  def encode([?M|t], r),                do: encode(t, <<r::bits,@morse[?M]::bits,0::3>>)
  def encode([?N|t], r),                do: encode(t, <<r::bits,@morse[?N]::bits,0::3>>)
  def encode([?O|t], r),                do: encode(t, <<r::bits,@morse[?O]::bits,0::3>>)
  def encode([?P|t], r),                do: encode(t, <<r::bits,@morse[?P]::bits,0::3>>)
  def encode([?Q|t], r),                do: encode(t, <<r::bits,@morse[?Q]::bits,0::3>>)
  def encode([?R|t], r),                do: encode(t, <<r::bits,@morse[?R]::bits,0::3>>)
  def encode([?S|t], r),                do: encode(t, <<r::bits,@morse[?S]::bits,0::3>>)
  def encode([?T|t], r),                do: encode(t, <<r::bits,@morse[?T]::bits,0::3>>)
  def encode([?U|t], r),                do: encode(t, <<r::bits,@morse[?U]::bits,0::3>>)
  def encode([?V|t], r),                do: encode(t, <<r::bits,@morse[?V]::bits,0::3>>)
  def encode([?W|t], r),                do: encode(t, <<r::bits,@morse[?W]::bits,0::3>>)
  def encode([?X|t], r),                do: encode(t, <<r::bits,@morse[?X]::bits,0::3>>)
  def encode([?Y|t], r),                do: encode(t, <<r::bits,@morse[?Y]::bits,0::3>>)
  def encode([?Z|t], r),                do: encode(t, <<r::bits,@morse[?Z]::bits,0::3>>)
  def encode('<SOS>' ++ t, r),          do: encode(t, <<r::bits,@morse["SOS"]::bits,0::3>>)
  def encode([ _|t], r),                do: encode(t, <<r::bits,@morse["ERROR"]::bits,0::3>>)

  import Shortcut

  def decode(bits),                                                      do: decode(bits, [])
  def decode(<<unquote(e(@morse["STARTING_SIGNAL"])),0::3,t::bits>>, r), do: decode(t, r)
  def decode(<<unquote(e(@morse[?0])),0::3,t::bits>>, r),                do: decode(t, [?0|r])
  def decode(<<unquote(e(@morse[?1])),0::3,t::bits>>, r),                do: decode(t, [?1|r])
  def decode(<<unquote(e(@morse[?2])),0::3,t::bits>>, r),                do: decode(t, [?2|r])
  def decode(<<unquote(e(@morse[?3])),0::3,t::bits>>, r),                do: decode(t, [?3|r])
  def decode(<<unquote(e(@morse[?4])),0::3,t::bits>>, r),                do: decode(t, [?4|r])
  def decode(<<unquote(e(@morse[?5])),0::3,t::bits>>, r),                do: decode(t, [?5|r])
  def decode(<<unquote(e(@morse[?6])),0::3,t::bits>>, r),                do: decode(t, [?6|r])
  def decode(<<unquote(e(@morse[?7])),0::3,t::bits>>, r),                do: decode(t, [?7|r])
  def decode(<<unquote(e(@morse[?8])),0::3,t::bits>>, r),                do: decode(t, [?8|r])
  def decode(<<unquote(e(@morse[?9])),0::3,t::bits>>, r),                do: decode(t, [?9|r])
  def decode(<<unquote(e(@morse[?.])),0::3,t::bits>>, r),                do: decode(t, [?.|r])
  def decode(<<unquote(e(@morse[?,])),0::3,t::bits>>, r),                do: decode(t, [?,|r])
  def decode(<<unquote(e(@morse[?:])),0::3,t::bits>>, r),                do: decode(t, [?:|r])
  def decode(<<unquote(e(@morse[??])),0::3,t::bits>>, r),                do: decode(t, [??|r])
  def decode(<<unquote(e(@morse[?'])),0::3,t::bits>>, r),                do: decode(t, [?'|r])
  def decode(<<unquote(e(@morse[?-])),0::3,t::bits>>, r),                do: decode(t, [?-|r])
  def decode(<<unquote(e(@morse[?/])),0::3,t::bits>>, r),                do: decode(t, [?/|r])
  def decode(<<unquote(e(@morse[?(])),0::3,t::bits>>, r),                do: decode(t, [?(|r])
  def decode(<<unquote(e(@morse[?)])),0::3,t::bits>>, r),                do: decode(t, [?)|r])
  def decode(<<unquote(e(@morse[?"])),0::3,t::bits>>, r),                do: decode(t, [?"|r])
  def decode(<<unquote(e(@morse[?=])),0::3,t::bits>>, r),                do: decode(t, [?=|r])
  def decode(<<unquote(e(@morse[?+])),0::3,t::bits>>, r),                do: decode(t, [?+|r])
  def decode(<<unquote(e(@morse[?@])),0::3,t::bits>>, r),                do: decode(t, [?@|r])
  def decode(<<unquote(e(@morse[?!])),0::3,t::bits>>, r),                do: decode(t, [?!|r])
  def decode(<<unquote(e(@morse[?;])),0::3,t::bits>>, r),                do: decode(t, [?;|r])
  def decode(<<unquote(e(@morse[?_])),0::3,t::bits>>, r),                do: decode(t, [?_|r])
  def decode(<<unquote(e(@morse[?&])),0::3,t::bits>>, r),                do: decode(t, [?&|r])
  def decode(<<unquote(e(@morse[?$])),0::3,t::bits>>, r),                do: decode(t, [?$|r])
  def decode(<<unquote(e(@morse[?A])),0::3,t::bits>>, r),                do: decode(t, [?A|r])
  def decode(<<unquote(e(@morse[?À])),0::3,t::bits>>, r),                do: decode(t, [?À|r])
  def decode(<<unquote(e(@morse[?B])),0::3,t::bits>>, r),                do: decode(t, [?B|r])
  def decode(<<unquote(e(@morse[?C])),0::3,t::bits>>, r),                do: decode(t, [?C|r])
  def decode(<<unquote(e(@morse[?Ç])),0::3,t::bits>>, r),                do: decode(t, [?Ç|r])
  def decode(<<unquote(e(@morse[?D])),0::3,t::bits>>, r),                do: decode(t, [?D|r])
  def decode(<<unquote(e(@morse[?E])),0::3,t::bits>>, r),                do: decode(t, [?E|r])
  def decode(<<unquote(e(@morse[?É])),0::3,t::bits>>, r),                do: decode(t, [?É|r])
  def decode(<<unquote(e(@morse[?È])),0::3,t::bits>>, r),                do: decode(t, [?È|r])
  def decode(<<unquote(e(@morse[?F])),0::3,t::bits>>, r),                do: decode(t, [?F|r])
  def decode(<<unquote(e(@morse[?G])),0::3,t::bits>>, r),                do: decode(t, [?G|r])
  def decode(<<unquote(e(@morse[?H])),0::3,t::bits>>, r),                do: decode(t, [?H|r])
  def decode(<<unquote(e(@morse[?I])),0::3,t::bits>>, r),                do: decode(t, [?I|r])
  def decode(<<unquote(e(@morse[?J])),0::3,t::bits>>, r),                do: decode(t, [?J|r])
  def decode(<<unquote(e(@morse[?K])),0::3,t::bits>>, r),                do: decode(t, [?K|r])
  def decode(<<unquote(e(@morse[?L])),0::3,t::bits>>, r),                do: decode(t, [?L|r])
  def decode(<<unquote(e(@morse[?M])),0::3,t::bits>>, r),                do: decode(t, [?M|r])
  def decode(<<unquote(e(@morse[?N])),0::3,t::bits>>, r),                do: decode(t, [?N|r])
  def decode(<<unquote(e(@morse[?O])),0::3,t::bits>>, r),                do: decode(t, [?O|r])
  def decode(<<unquote(e(@morse[?P])),0::3,t::bits>>, r),                do: decode(t, [?P|r])
  def decode(<<unquote(e(@morse[?Q])),0::3,t::bits>>, r),                do: decode(t, [?Q|r])
  def decode(<<unquote(e(@morse[?R])),0::3,t::bits>>, r),                do: decode(t, [?R|r])
  def decode(<<unquote(e(@morse[?S])),0::3,t::bits>>, r),                do: decode(t, [?S|r])
  def decode(<<unquote(e(@morse[?T])),0::3,t::bits>>, r),                do: decode(t, [?T|r])
  def decode(<<unquote(e(@morse[?U])),0::3,t::bits>>, r),                do: decode(t, [?U|r])
  def decode(<<unquote(e(@morse[?V])),0::3,t::bits>>, r),                do: decode(t, [?V|r])
  def decode(<<unquote(e(@morse[?W])),0::3,t::bits>>, r),                do: decode(t, [?W|r])
  def decode(<<unquote(e(@morse[?X])),0::3,t::bits>>, r),                do: decode(t, [?X|r])
  def decode(<<unquote(e(@morse[?Y])),0::3,t::bits>>, r),                do: decode(t, [?Y|r])
  def decode(<<unquote(e(@morse[?Z])),0::3,t::bits>>, r),                do: decode(t, [?Z|r])
  def decode(<<unquote(e(@morse[?\n])),0::3,t::bits>>, r),               do: decode(t, [?\n|r])
  def decode(<<unquote(e(@morse[?\s])),0::3,t::bits>>, r),               do: decode(t, [?\s|r])
  def decode(<<unquote(e(@morse["ERROR"])),0::3,t::bits>>, r),           do: decode(t, [?�|r])
  def decode(<<unquote(e(@morse["SOS"])),0::3,t::bits>>, r),             do: decode(t, '>SOS<' ++ r)
  def decode(<<unquote(e(@morse["END_OF_WORK"]))>>, r),                  do: Enum.reverse(r)    
end

printf = fn
  :pretty, <<bits::bits>> -> :io.format("~s\n",  [(for <<x::1 <- bits>>, do: ?0+x)])
  :pretty, charlist       -> :io.format("~ts\n", [charlist])
  :terms,  <<bits::bits>> -> :io.format("~w.\n", [bits])
  :terms,  charlist       -> :io.format("~p.\n", [charlist])
end

code = fn
  <<bits::bits>> -> Morse.decode(bits)
  charlist       -> Morse.encode(:string.to_upper(charlist))
end

main2 = fn(print, file) ->
  {:ok, content} = :file.consult(file)
  content
  |> Stream.map(fn(x) -> code.(x) end)
  |>   Enum.map(fn(x) -> printf.(print, x) end)
end

main1 = fn
  [file]       -> main2.(:pretty, file)
  ["-t", file] -> main2.(:terms, file) 
end

main1.(System.argv)
