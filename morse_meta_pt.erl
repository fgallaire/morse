% Copyright (C) 2019, Florent Gallaire <f@gallai.re>
% MIT license

-module(morse_meta_pt).
-export([parse_transform/2]).

parse_transform(Ast, _Opt) ->
        Morse = maps(lists:nth(4, Ast)),
        Signal = maps(lists:nth(5, Ast)),
        Replace = maps(lists:nth(6, Ast)),
        [parse(Tuple, [Morse,Signal,Replace]) || Tuple <- Ast].

int_size(Bitstring) -> Size = bit_size(Bitstring),
                       <<Int:Size>> = Bitstring,
                       {Int, Size}.

maps({function, _, _, 1, [{_, _, _, _, [{_, _, _, Map}, _]}]}) ->
        element(2, erl_eval:expr(Map, erl_eval:new_bindings())).

parse({function, Line, Function, 2, Clauses}, Maps) when Function =:= encode; Function =:= decode ->
	{function, Line, Function, 2, lists:flatten([parse(Clause, Maps) || Clause <- Clauses])};

parse({clause,L, [{_,_,_X,T}|R],Guard,[{_,_,{_,_,encode}=En,[{_,_,_,{_,_,{_,_,replace}=Re,[_X]},T},R2]}]}, [_,_,Replace]) ->
        [{clause,L,[{cons,L,{char,L,Key},T}|R],Guard,[{call,L,En,[{op,L,'++',{call,L,Re,[{char,L,Key}]},T},R2]}]}
        || {Key, _} <- maps:to_list(Replace)];

parse({clause,L, [{_,_,_X,T}|R],Guard,[{_,_,{_,_,encode}=En,[T,{_,_, [B1,{_,_,{_,_,{_,_,morse}=Mo,[_X]},D,Bi}, B2]}]}]}, [Morse,_,_]) ->
        [{clause,L,[{cons,L,{char,L,Key},T}|R],Guard,[{call,L,En, [T,{bin,L, [B1,{bin_element,L,{call,L,Mo,[{char,L,Key}]},D,Bi},B2]}]}]}
        || {Key, _} <- maps:to_list(Morse)];

parse({clause, L, [{_,_, [{_,_ , {_,_ , {_, _, morse}, [_X]}, _, D}|T1]}|T2],Guard,[{_,_,{_,_,decode}=De,[T3,{cons,_,_X,R}]}]},[Morse,_,_]) ->
        [{clause, L, [{bin, L, [{bin_element, L, {integer, L, Int},{integer,L,Size},D}|T1 ]}|T2], Guard, [{call,L,De,[T3,{cons,L,{char,L,Key},R}]}]}
        || {Key, {Int, Size}} <- [{Key, int_size(Value)} || {Key, Value} <- maps:to_list(Morse)]];

parse({clause, L, [{_, _, [{_, _, {_, _, {_, _, signal}, [{_, _, Val}]}, _, D}|T1]}|T2], Guard, Expr}, [_,Signal,_]) ->
        {Int, Size} = int_size(maps:get(Val, Signal)),
        {clause, L, [{bin, L,[{bin_element, L, {integer, L, Int}, {integer, L, Size}, D}|T1]}|T2], Guard, Expr};

parse(Tuple, _) -> Tuple.
