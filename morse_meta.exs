#!/usr/bin/env elixir
# Copyright (C) 2019, Florent Gallaire <f@gallai.re>
# MIT license

defmodule Morse do
  @digits '0123456789'
  @fractions '½⅓⅔¼¾⅕⅖⅗⅘⅙⅚⅐⅛⅜⅝⅞⅑⅒'
  @morse %{ ?0 => <<0b1110111011101110111::19>>,
            ?1 => <<0b10111011101110111::17>>,
            ?2 => <<0b101011101110111::15>>, 
            ?3 => <<0b1010101110111::13>>,
            ?4 => <<0b10101010111::11>>,
            ?5 => <<0b101010101::9>>,
            ?6 => <<0b11101010101::11>>,
            ?7 => <<0b1110111010101::13>>,
            ?8 => <<0b111011101110101::15>>,
            ?9 => <<0b11101110111011101::17>>,

            ?A => <<0b10111::5>>,
            ?À => <<0b101110111010111::15>>,
            ?B => <<0b111010101::9>>,
            ?C => <<0b11101011101::11>>,
            ?Ç => <<0b1110101110101::13>>,
            ?D => <<0b1110101::7>>,
            ?E => <<0b1::1>>,
            ?É => <<0b10101110101::11>>,
            ?È => <<0b1011101010111::13>>,
            ?F => <<0b101011101::9>>,
            ?G => <<0b111011101::9>>,
            ?H => <<0b1010101::7>>,
            ?I => <<0b101::3>>,
            ?J => <<0b1011101110111::13>>,
            ?K => <<0b111010111::9>>,
            ?L => <<0b101110101::9>>,
            ?M => <<0b1110111::7>>,
            ?N => <<0b11101::5>>,
            ?O => <<0b11101110111::11>>,
            ?P => <<0b10111011101::11>>,
            ?Q => <<0b1110111010111::13>>,
            ?R => <<0b1011101::7>>,
            ?S => <<0b10101::5>>,
            ?T => <<0b111::3>>,
            ?U => <<0b1010111::7>>,
            ?V => <<0b101010111::9>>,
            ?W => <<0b101110111::9>>,
            ?X => <<0b11101010111::11>>,
            ?Y => <<0b1110101110111::13>>,
            ?Z => <<0b11101110101::11>>,

            ?.  => <<0b10111010111010111::17>>,
            ?,  => <<0b1110111010101110111::19>>,
            ?:  => <<0b11101110111010101::17>>,
            ??  => <<0b101011101110101::15>>,
            ?'  => <<0b1011101110111011101::19>>,
            ?-  => <<0b111010101010111::15>>,
            ?"  => <<0b101110101011101::15>>,
            ?@  => <<0b10111011101011101::17>>,
            ?!  => <<0b1110101110101110111::19>>,
            ?;  => <<0b11101011101011101::17>>,
            ?=  => <<0b1110101010111::13>>,
            ?+  => <<0b1011101011101::13>>,
            ?/  => <<0b1110101011101::13>>,
            ?(  => <<0b111010111011101::15>>,
            ?)  => <<0b1110101110111010111::19>>,
            ?_  => <<0b10101110111010111::17>>,
            ?&  => <<0b10111010101::11>>,
            ?$  => <<0b10101011101010111::17>>,
            ?\n => <<0b10111010111::11>>,
            ?\s => <<0b0::1>>,
  }
  @signal %{ "STARTING_SIGNAL" => <<0b111010111010111::15>>,
             "END_OF_WORK"     => <<0b101010111010111::15>>,
             "SOS"             => <<0b10101011101110111010101::23>>,
             "ERROR"           => <<0b101010101010101::15>>
  }
  @replace %{ ?×  => 'X',
              ?′  => '\'',
              ?″  => '\'\'',
              ?“  => '"',
              ?”  => '"',
              ?%  => '0/0',
              ?‰  => '0/00',
              ?½  => '1/2',
              ?⅓  => '1/3',
              ?⅔  => '2/3',
              ?¼  => '1/4',
              ?¾  => '3/4',
              ?⅕  => '1/5',
              ?⅖  => '2/5',
              ?⅗  => '3/5',
              ?⅘  => '4/5',
              ?⅙  => '1/6',
              ?⅚  => '5/6',
              ?⅐  => '1/7',
              ?⅛  => '1/8',
              ?⅜  => '3/8',
              ?⅝  => '5/8',
              ?⅞  => '7/8',
              ?⅑  => '1/9',
              ?⅒  => '1/10'
  }
        
  def encode(charlist),                do: do_encode(charlist, <<>>)
  defp do_encode([], r),               do: <<@signal["STARTING_SIGNAL"]::bits,0::3,r::bits,@signal["END_OF_WORK"]::bits>>
  defp do_encode([x,y|t], r) when x in @digits and (y in @fractions or y in '%‰')
                               or y in @digits and  x in @fractions,
                                       do: do_encode([x,?-,y|t], r)
  defp do_encode('<SOS>' ++ t, r),     do: do_encode(t, <<r::bits,@signal["SOS"]::bits,0::3>>)
  for {k, v} <- @replace do
    defp do_encode([unquote(k)|t], r), do: do_encode(unquote(v) ++ t, r)
  end
  for {k, v} <- @morse do
    defp do_encode([unquote(k)|t], r), do: do_encode(t, <<r::bits,unquote(Macro.escape(v)),0::3>>)
  end
  defp do_encode([_|t], r),            do: do_encode(t, <<r::bits,unquote(Macro.escape(@signal["ERROR"])),0::3>>)

  def decode(bits),                                                                      do: do_decode(bits, [])
  defp do_decode(<<unquote(Macro.escape(@signal["STARTING_SIGNAL"])),0::3,t::bits>>, r), do: do_decode(t, r)
  defp do_decode(<<unquote(Macro.escape(@signal["ERROR"])),0::3,t::bits>>, r),           do: do_decode(t, [?�|r])
  defp do_decode(<<unquote(Macro.escape(@signal["SOS"])),0::3,t::bits>>, r),             do: do_decode(t, '>SOS<' ++ r)
  defp do_decode(<<unquote(Macro.escape(@signal["END_OF_WORK"]))>>, r),                  do: Enum.reverse(r)    
  for {k, v} <- @morse do
    defp do_decode(<<unquote(Macro.escape(v)),0::3,t::bits>>, r),                        do: do_decode(t, [unquote(k)|r])
  end

  def code(<<bits::bits>>), do: decode(bits)
  def code(charlist),       do: encode(:string.to_upper(charlist))

  def printf(:pretty, <<bits::bits>>), do: :io.format("~s\n",  [(for <<x::1 <- bits>>, do: ?0+x)])
  def printf(:pretty, charlist),       do: :io.format("~ts\n", [charlist])
  def printf(:terms,  <<bits::bits>>), do: :io.format("~w.\n", [bits])
  def printf(:terms,  charlist),       do: :io.format("~p.\n", [charlist])

  defp do_main(print, file) do
    {:ok, content} = :file.consult(file)
    content
    |> Stream.map(&code(&1))
    |>   Enum.map(&printf(print, &1))
  end

  def main([file]),       do: do_main(:pretty, file)
  def main(["-t", file]), do: do_main(:terms, file)

end

Morse.main(System.argv)
